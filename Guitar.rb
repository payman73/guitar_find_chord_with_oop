#!usr/bin/env ruby


class Guitar
  attr_accessor :fret_number , :string_name
  
  BOARD_STRING  = {e:["F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" ,"E" , "F" ] , b:["C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C"] , g:["G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A"] , d:["D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E"] , a:["A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#" , "B" , "C" , "C#" , "D" , "D#" , "E" , "F" , "F#" , "G" , "G#" , "A" , "A#"]}


  def find_chord(string_name: self.string_name , fret_number: self.fret_number)
    @string_name  = string_name
    @fret_number  = fret_number
    x  = "\n\n\t\tI can not find the desired option! please try again.\n\n"
    BOARD_STRING.keys.each do |item|
      if self.string_name.upcase == item.to_s.upcase && self.fret_number <= BOARD_STRING[item].length
        x  = "\n\n\t\tThe name of the note you want ~> #{BOARD_STRING[item][self.fret_number]}\n\n"
      end
    end
    return x
  end
end



